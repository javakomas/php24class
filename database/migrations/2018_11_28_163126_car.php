<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Car extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('car', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_number', 8);
            $table->string('brand', 32);
            $table->string('model', 32);
            $table->unsignedInteger('owner_id');
            $table->timestamps();
            $table->foreign('owner_id')->references('id')->on('owner');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car');
    }
}
