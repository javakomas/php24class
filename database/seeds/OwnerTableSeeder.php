<?php

use Illuminate\Database\Seeder;

class OwnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('owner')->insert([
            'name' => str_random(5),
            'surname' => str_random(5),
            'id' => 1
 
        ]);       
        
        DB::table('owner')->insert([
            'name' => str_random(5),
            'surname' => str_random(5),
            'id' => 2
 
        ]);
    }
}
