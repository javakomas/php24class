@foreach ($owners as $owner)
    <h3>
    {{ $owner->name}}{{ $owner->surname}}
    @foreach ($owner->cars() as $car)
    <ul>
    <li>{{ $car->reg_number}}</li>
    <li>{{ $car->model}}</li>
    </ul>
        @endforeach
    </h3>
 @endforeach