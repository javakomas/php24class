<?php 

namespace App\Http\Controllers;

use App\Owner;
class OwnerController extends Controller 
{
    public function list()
    {
        return view('owner_list',[
            'owners' => Owner::all()
        ]);
    }
}